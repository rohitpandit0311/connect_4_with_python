import pygame
import random
import copy
import pprint
import math
import time


pygame.init()

# display size
display_height = 600
display_width = 700

# color defination
black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
green = (0, 255, 0)


# game initials
game_display = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption("Connect-4 with AI")
game_display.fill(white)
clock = pygame.time.Clock()


# function for setting the initial user interface
def game_ui():
    for i in range(7, 0 ,-1):
        pygame.draw.line(game_display, black, [i*100, 0], [i*100, 600], 1)

    for j in range(6, 0, -1):
        pygame.draw.line(game_display, black, [0, j*100], [700, j*100], 1)

    pygame.display.update()


# function for declaring the winner
def winner(value):

    if value == 0:
        message_display("AI won!")

    else:
        message_display("You won!")


def text_object(text, font):
    textsurface = font.render(text, True, black)
    return textsurface, textsurface.get_rect()


# function for displayinf the message
def message_display(text):
    largeText = pygame.font.Font("freesansbold.ttf", 115)
    TextSurf, TextRect = text_object(text, largeText)
    TextRect.center = ((display_width/2), (display_height/2))
    game_display.blit(TextSurf, TextRect)

    pygame.display.update()
    time.sleep(6)
    pygame.quit()
    quit()


# function to capture the location of the mouse clicked by a player
def get_mouse_click(click, mouse):

    if mouse[0] < 100:
        if click[0] == 1:
            return 0

    elif mouse[0] < 200:
        if click[0] == 1:
            return 1

    elif mouse[0] < 300:
        if click[0] == 1:
            return 2

    elif mouse[0] < 400:
        if click[0] == 1:
            return 3

    elif mouse[0] < 500:
        if click[0] == 1:
            return 4

    elif mouse[0] < 600:
        if click[0] == 1:
            return 5

    elif mouse[0] < 700:
        if click[0] == 1:
            return 6


# function to check if the move is valid or not
def check_valid_move(empty, position):
    if empty[position] >= 0:
        return True

    else:
        return False


# function to update game_matrix
def update_game_matrix(game_matrix, empty, position, value):
    game_matrix[empty[position]][position] = value
    empty[position] -= 1


# function to update the ui of the game
def update_game_ui(position, empty, color):
    pygame.draw.circle(game_display, color, [position*100 + 50, (empty[position])*100 + 50], 45)


# fuction to check game win condition
def check_win(game_matrix, value):

    # checking horizontally
    for i in range(6, -1, -1):
        for j in range(3):
            if game_matrix[j][i] == value and game_matrix[j+1][i] == value and game_matrix[j+2][i] == value and game_matrix[j+3][i] == value:

                return True
    # checking vertically
    for i in range(5, -1, -1):
        for j in range(4):
            if game_matrix[i][j] == value and game_matrix[i][j+1] == value and game_matrix[i][j+2] == value and game_matrix[i][j+3] == value:

                return True

    # checking diagonally
    for i in range(5, 2, -1):
        for j in range(4):
            if game_matrix[i][j] == value and game_matrix[i-1][j+1] == value and game_matrix[i-2][j+2] == value and game_matrix[i-3][j+3] == value:

                return True

    # checking reverse diagonally
    for i in range(5, 2, -1):
        for j in range(6, 2, -1):
            if game_matrix[i][j] == value and game_matrix[i-1][j-1] == value and game_matrix[i-2][j-2] == value and game_matrix[i-3][j-3] == value:

                return True


# function for the first player
def player1(game_matrix, empty, position):

    value = 1
    color = green

    if check_valid_move(empty, position) is True:
        update_game_ui(position, empty, color)
        update_game_matrix(game_matrix, empty, position, value)
        pygame.display.update()
        pprint.pprint(game_matrix)
        won = check_win(game_matrix, value)
        if won:
            winner(value)


# function to mark score in set_score()
def mark_score(section, value):
    score = 0

    if section.count(value) == 4:
        score += 100

    elif section.count(value) == 3 and section.count(8) == 1:
        score += 5

    elif section.count(value) == 2 and section.count(8) == 2:
        score += 2

    if section.count(value + 1) == 3 and section.count(8) == 1:
        score -= 4

    return score


# function for assigning score to the move
def set_score(game_matrix, value):
    score = 0

    # priotrizing central moves
    center_array = [game_matrix[i][3] for i in range(6)]
    center_count = center_array.count(value)
    score += center_count*6

    # horizontally
    for r in range(6):
        row_array = [game_matrix[r]]
        for c in range(4):
            section = row_array[c:c+4]
            score += mark_score(section, value)

    # vertically
    for c in range(7):
        col_array = [game_matrix[i][c] for i in range(6)]
        for r in range(3):
            section = col_array[r:r+4]
            score += mark_score(section, value)

    # diagonally
    for r in range(3):
        for c in range(4):
            section = [game_matrix[r+i][c+i] for i in range(4)]
            score += mark_score(section, value)

    # reverse diagonal
    for c in range(4):
        for r in range(3):
            section = [game_matrix[r+3-i][c+i] for i in range(4)]
            score += mark_score(section, value)

    return score


# function for checking the terminal node
def is_terminal_node(game_matrix, empty):
    return check_win(game_matrix, 0) or check_win(game_matrix, 1) or len(get_valid_locations(empty)) == 0


# function for the minimax algorithm
def minimax(game_matrix, empty, alpha, beta, depth, maximizingPlayer):
    valid_locations = get_valid_locations(empty)
    is_terminal = is_terminal_node(game_matrix, empty)

    if depth == 0 or is_terminal:
        if is_terminal:
            if check_win(game_matrix, 0):
                return None, 1000000000000000
            elif check_win(game_matrix, 1):
                return None, -1000000000000000
            else:
                return None, 0

        else:
            return None, set_score(game_matrix, 0)

    if maximizingPlayer:
        valv = -math.inf
        column = random.choice(valid_locations)

        for col in valid_locations:
            new_empty = copy.deepcopy(empty)
            new_matrix = copy.deepcopy(game_matrix)
            update_game_matrix(new_matrix, new_empty, col, 0)
            new_score = minimax(new_matrix, new_empty, alpha, beta, depth-1, False)[1]

            if new_score > valv:
                valv = new_score
                column = col
            alpha = max(valv, alpha)

            if alpha >= beta:
                break

        return column, valv

    else:
        valv = math.inf
        column = random.choice(valid_locations)

        for col in valid_locations:
            new_empty = copy.deepcopy(empty)
            new_matrix = copy.deepcopy(game_matrix)
            update_game_matrix(new_matrix, new_empty, col, 1)
            new_score = minimax(new_matrix, new_empty, alpha, beta, depth-1, True)[1]

            if new_score < valv:
                valv = new_score
                column = col
            beta = min(beta, valv)

            if alpha >= beta:
                break

        return column, valv


# function to get the available valid locations
def get_valid_locations(empty):
    valid_locations = []

    for c in range(7):
        if check_valid_move(empty, c):
            valid_locations.append(c)

    return valid_locations


# function for the second player
def ai_player(game_matrix, empty):

    value = 0
    color = red

    # position = random.randint(0, 6)

    position = minimax(game_matrix, empty, -math.inf, math.inf,  5, True)[0]
    print(position)

    update_game_ui(position, empty, color)
    update_game_matrix(game_matrix, empty, position, value)
    pprint.pprint(game_matrix)
    pygame.display.update()
    won = check_win(game_matrix, value)
    if won:
        winner(value)


# The main game loop

def game_loop():

    empty = [5, 5, 5, 5, 5, 5, 5]

    game_matrix = [[8]*7 for _ in range(6)]
    game_ui()
    tre = 0
    while tre < 21:
        tre = -1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            clicked = pygame.mouse.get_pressed()
            if clicked[0] == 1:

                mouse = pygame.mouse.get_pos()
                click = pygame.mouse.get_pressed()
                position = get_mouse_click(click, mouse)

                player1(game_matrix, empty, position)
                print("-------------------------------------------")
                ai_player(game_matrix, empty)


game_loop()

pygame.quit()
quit()
